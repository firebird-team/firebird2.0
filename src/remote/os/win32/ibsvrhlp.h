/*
 * The contents of this file are subject to the Interbase Public
 * License Version 1.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy
 * of the License at http://www.Inprise.com/IPL.html
 *
 * Software distributed under the License is distributed on an
 * "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * The Original Code was created by Inprise Corporation
 * and its predecessors. Portions created by Inprise Corporation are
 * Copyright (C) Inprise Corporation.
 *
 * All Rights Reserved.
 * Contributor(s): ______________________________________.
 */
const WPARAM ibsp_Server_Information_Properties	= 1000;
const WPARAM ibsp_IB_Settings_Properties		= 2000;
//const WPARAM ibsp_OS_Settings_Properties		= 3000;
const DWORD ibs_image_path				= 4000;
const DWORD ibs_version					= 4005;
const DWORD ibs_num_dbs_attached		= 4010;
const DWORD ibs_num_dbs					= 4020;
const DWORD ibs_refresh					= 4030;
const DWORD ibs_db_cache				= 4050;
const DWORD ibs_client_map_size			= 4060;
const DWORD ibs_modify					= 4070;
const DWORD ibs_reset					= 4080;
const DWORD ibs_min_process_work_set	= 4090;
const DWORD ibs_process_priority		= 5000;
const DWORD ibs_server_icon				= 5010;
const DWORD ibs_capabilities			= 5020;
const DWORD ibs_prodname				= 5030;





