sk.MyAppName=Datab�zov� server Firebird %1
sk.ServerInstall=�pln� in�tal�cia Serveru vr�tane n�strojov pre spr�vu
sk.SuperServerInstall=�pln� in�tal�cia Super Serveru vr�tane n�strojov pre spr�vu
sk.ClassicServerInstall=�pln� in�tal�cia Classic Serveru vr�tane n�strojov pre spr�vu
sk.DeveloperInstall=Klientsk� in�tal�cia vr�tane n�strojov pre spr�vu
sk.ClientInstall=Minim�lna klientsk� in�tal�cia
sk.CustomInstall=Volite�n� in�tal�cia
sk.SuperServerComponent=Super Server
sk.ClassicServerComponent=Classic Server
sk.ServerComponent=Datab�zov� server Firebird
sk.DevAdminComponent=N�stroje pre spr�vu
sk.ClientComponent=Klientsk� kni�nica
sk.UseGuardianTask=Pou�i� bezpe�n� spustenie pomocou &Guardian?
sk.UseApplicationTaskMsg=Sp���a� ako samostatn� &aplik�ciu?
sk.TaskGroupDescription=Sp�sob sp���ania Firebird serveru:
sk.UseServiceTask=Sp���a� ako &slu�bu?
sk.AutoStartTask=Automaticky sp���a� pri �tarte &po��ta�a?
sk.CopyFbClientToSysTask=Kop�rova� klientsk� kni�nicu firebirdu do <sys> adres�ra?
sk.CopyFbClientAsGds32Task=Vytvori� klientsk� kni�nicu GDS32.DLL pre podporu star��ch aplik�ci�?
sk.InstallCPLAppletTask="In�talova� pr�davn� modul do kontroln�ho panelu?"
sk.instreg=Aktualiz�cia registra
sk.instclientCopyFbClient=Kop�rovanie klientskej kni�nice do <sys> adres�ra
sk.instclientGenGds32=Vytv�ranie gds32.dll v <sys> adres�ri
sk.instclientDecLibCountGds32="Zn�enie po�tu instanci� pre sdie�an� kni�nicu gds32 a jej odstr�nenie ak je to mo�n�."
sk.instclientDecLibCountFbClient="Zn�enie po�tu instanci� pre sdie�an� kni�nicu fbclient a jej odstr�nenie ak je to mo�n�."
sk.instsvcSetup=Nastavenie slu�by
sk.instsvcStartQuestion=Spusti� teraz slu�bu Firebird ?
sk.instsvcStartMsg=Sp���am server
sk.instsvcStopMsg="Zastavujem slu�bu Firebird"
sk.instsvcRemove="Odstr�nenie slu�by Firebird"
sk.instappStartQuestion=Spusti� teraz Firebird ?
sk.instappStartMsg=Sp���am server

sk.InstallSummarySuffix1=In�talovan� verzia Firebirdu sa nastav� ako v�chodiskov�.
sk.InstallSummarySuffix2=Upozornenie: Pokra�ovanie v in�tal�cii
sk.InstallSummarySuffix3=m��e vies� k nepredv�date�n�mu chovaniu
sk.InstallSummarySuffix4=ak je na po��ta�i nain�talovan� nov�ia verzia firebirdu.

sk.InstallSummaryCancel1= Pokra�ovan�m v in�tal�cii sa Firebird nain�taluje, ale nenakonfiguruje
sk.InstallSummaryCancel2= a budete musie� dokon�i� in�tal�ciu ru�ne.
sk.InstallSummaryCancel3= Chcete preru�i� in�tal�ciu ?

sk.InstalledProducts=Na po��ta�i je u� nain�talovan� Firebird %s alebo Interbase %s.
sk.InstalledProdCountSingular=verzia m�
sk.InstalledProdCountPlural=verzie maj�

sk.FbRunning1=Prvotne in�talovan� %s datab�zov� server je spusten�.
sk.FbRunning2= Predt�m, ne� budete pokra�ova�, 
sk.FbRunning3=        ho mus�te zastavi�.

sk.PreviousInstallBroken=   (in�tal�cia zrejme nie je funk�n�.)
sk.PreviousInstallGood=   (in�tal�cia je zrejme funk�n�.)

sk.IconReadme=Firebird %1 Readme
sk.IconUninstall=Uninstall Firebird %1
sk.ReleaseNotes= release notes. (Requires Acrobat Reader.)
sk.InstallationGuide= installation guide. (Requires Acrobat Reader.)
sk.BugFixes= bug fixes. (Requires Acrobat Reader.)
sk.Uninstall=Uninstall Firebird %1
sk.Winsock2Web1=Winsock 2 nie je nain�talovan�.
sk.Winsock2Web2=Chcete zobrazi� domovsk� str�nku Winsock 2?
sk.NoWinsock2=Ne� budete pokra�ova� nain�talujte si pros�m Winsock 2 Update 
sk.MSWinsock2Update=http://www.microsoft.com/windows95/downloads/contents/WUAdminTools/S_WUNetworkingTools/W95Sockets2/Default.asp

sk.RunCSNoGuardian=Spusti� Firebird Classic Server (bez str�cu)
sk.RunSSNoGuardian=Spusti� Firebird Super Server (bez str�cu)
sk.RunSSWithGuardian=Spusti� Firebird Super Server (so str�com)
sk.RunCSWithGuardian=Spusti� Firebird Classic Server (so str�com)
sk.RunISQL=Spusti� n�stroj pr�kazov�ho riadku Interactive SQL tool

sk.ReadmeFile=readme.txt
sk.InstallationReadmeFile=installation_readme.txt
