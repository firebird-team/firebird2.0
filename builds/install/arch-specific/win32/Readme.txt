
==================================
Firebird 2.0.5       (Win32 Build)
==================================


o Introduction
o Intended Users
o Features in this release (all platforms)
o Bugs fixed in this release
o Installation
o Known Issues
o Reporting Bugs
o Requesting New Features


Introduction
============

Welcome to Firebird 2.0


Intended Users
==============

Firebird 2.0 has undergone extensive testing and is
intended for widespread production use. However, users
are recommended to follow standard practices before
deploying this release on a production server. ie:

 o Please make sure you read the installation
   readme and the release notes.

 o If you have data you value remember to back it up
   prior to installing this release.

 o It is recommended that you remove any previous
   version prior to installation. Uninstallation
   preserves configuration files and log files.

 o It is recommended that you carry out your own
   tests in a development environment prior to
   production deployment.


Features in this release (all platforms)
========================================

See doc/Firebird_v2.0.5.ReleaseNotes.pdf for more
information.


Bugs fixed in this release (all platforms)
==========================================

See doc/Firebird_v2.0.5.ReleaseNotes.pdf for more
information.


Installing the self-installing executable
=========================================

Please run the executable and read the accompanying
installation instructions that are contained within the
setup wizard.


Known Issues
============

 o The security database has changed! See the DOC
   directory for more details. Users who wish to
   migrate an old security database to the new format
   can use the upgrade script in misc/upgrade/security


Reporting Bugs
==============

Before you report a bug:

 o Check you know how Firebird works.
   Maybe it is not a bug at all.

 o Perhaps some has already reported this? Browse
   existing bug reports here:

	http://tracker.firebirdsql.org/secure/BrowseProject.jspa

 o If in doubt why not discuss the problem on the
   Firebird-devel list? You can subscribe here:

	http://lists.sourceforge.net/lists/listinfo/firebird-devel

   and the list is viewable via a newsgroup interface
   here:

	news://news.atkin.com/



From the Firebird team.

