Firebird Adatb�zis Szerver 2.0.5
===================================================

  ** FONTOS **

  Az ODS adatt�rol�si strukt�ra megv�ltozott
  a Firebird 1.5 �ta. L�sd az al�bbi megjegyz�seket,
  hogy hogyan telep�thet� az �j verzi� a r�gebbi
  Firebird verzi�kra.

  ** ****** **

Ez a dokumentum egy telep�t�si seg�dlet a Win32
platformon m�k�d� Firebird 2.0.5 telep�t�s�hez.
A telep�t�si seg�dlet els�sorban a telep�t�s menet�t
�rja le, s nem a Firebird 2.0.5-r�l sz�l
�ltal�noss�gban.


Tartalomjegyz�k
---------------

o Miel�tt telep�ten�
o Az �j OSD 11.0-r�l
o Telep�t�s megl�v� Firebird 1.5 mell�
o Telep�t�s m�r megl�v� Firebird 2.0 eset�n
o A telep�t� �j szolg�ltat�sai
o Ismert telep�t�si probl�m�k
o Elt�vol�t�s
o Megjegyz�sek
o Telep�t�s k�tegelt (batch) f�jlb�l


Miel�tt telep�ten�
------------------

Miel�tt telep�ti ezt a v�ltozatot, aj�nlatos
ELT�VOL�TANI a Firebird illetve az InterBase �sszes
el�z� verzi�j�t. K�l�n�sen fontos �s ellen�rizend�,
hogy a fbclient.dll �s gds32.dll f�jlok elt�vol�t�sra
ker�ljenek a <system32> mapp�b�l.


Az �j OSD 11-r�l
----------------

A Firebird 2.0  v�ltozatban haszn�latos �j ODS 11.0
adatt�rol�si strukt�ra teljess�ggel inkompatibilis az
el�z� verzi�kban alkalmazott ODS 11.0 adatt�rol�si
strukt�r�val.

Amennyiben ez a Firebird 2.0 v�ltozat els� telep�t�se
ezen a rendszeren, de m�r telep�tette a Firebird 2
alfa v�ltozatait is, akkor aj�nljuk, hogy v�lasszon
egy tiszta, �res k�nyvt�rat a telep�t�shez. Ennek oka,
hogy az elt�vol�t�si folyamat h�trahagyja a security2
adatb�zist. Mivel ez a r�gebbi v�ltozat nem
kompatibilis ODS adatt�rol�si strukt�r�j�, �gy nem
lesz lehet�s�g a szerverhez t�rt�n� kapcsol�d�shoz.


Telep�t�s megl�v� Firebird 1.5 mell�
------------------------------------

A biztons�gi adatb�zis megv�ltozott Az �j adatb�zis
neve security2.fdb lett �s meger�s�tett
jelsz�titkos�t�si algoritmus ker�lt benne alkalmaz�sra.
Tov�bbi, az �j biztons�gi funkci�kkal kapcsolatos
inform�ci�kat a kiad�si jegyzetben tal�lhat.

Az �j adatt�rol�si strukt�ra �s a megv�ltoztatott
jelsz�titkos�t�si elj�r�s sz�mos �tt�r�si probl�m�t
vet fel els�sorban azok sz�m�ra akik nagysz�m�
felhaszn�l�val rendelkez� adatb�zisokat
adminisztr�lnak. A telep�t� nem tesz k�s�rletet az
�tt�r�s seg�t�s�re, ezt k�zileg kell megtenni. A
lehets�ges �tt�r�si utakr�l m�g t�bbet olvashat a
misc/ upgrade/security/ helyen.


Telep�t�s m�r megl�v� Firebird 2.0 eset�n
-----------------------------------------

Az ODS adatt�rol�si strukt�ra megv�ltoz�sa mellett
jegyezz�k meg, hogy a telep�t� nem k�pes
automatikusan felismerni a Firebird 2.0 Alfa, B�ta
�ppen szervizk�nt fut� v�ltozatait. Amennyiben a
c�lg�pen a Firebird 2.0 - nem stabil - v�ltozata fut,
�gy aj�nlatos a szervert le�ll�tani, miel�tt
megpr�b�lja telep�teni a Firebird 2.0-t. Ellenkez�
esetben figyelmeztet�st csak akkor fog kapni, ha az
�ppen fut� f�jl fel�l�r�s�ra ker�lne a sor. Ez
elker�lhet�, ha a r�gi verzi�t telep�t�s el�tt
elt�vol�tja.


A telep�t� �j szolg�ltat�sai
----------------------------

o Ha egy v�ltozatlan firebird.conf l�tezik a telep�t�s
  mapp�j�ban, akkor az egy alap�rtelmezett konfigur�ci�s
  �llom�ny ker�l ment�sre az al�bbi n�ven:
  firebird.conf.default

  A eredetileg l�tez� firebird.conf f�jl �rintetlen marad,
  ennek eredm�nyek�ppen a hozz�f�r�si korl�toz�sok
  kezel�s�t a friss�t�s ut�n kell esetlegesen
  elv�gezni.

o A telep�t� imm�ron sz�mos nyelven haszn�lhat�. A
  t�bbnyelv� t�mogat�st jelenleg csak a telep�t�si
  elj�r�s sor�n �rhet� el.

  MEGJEGYZ�S: A telep�t�s nyelv�t a honos�tott Windows
  verzi� adja meg. Ez azt jelent hogy egy nyugat-
  eur�pai Windows felhaszn�l�ja sz�m�ra nem �rhet� el
  p�ld�ul a szl�v nyelv� telep�t�s.


Ismert telep�t�si probl�m�k
---------------------------

Jelenleg nincsen ismert telep�t�si probl�ma.


Elt�vol�t�s
-----------

o Aj�nlatos, hogy telep�tett alkalmaz�st szab�lyosan, a
  mell�kelt elt�vol�t� programmal t�vol�tsa el. Ezt a
  programot elind�thatja a Vez�rl�pultr�l.
  Alternat�vak�nt elind�thatja a unins000.exe
  elt�vol�t� alkalmaz�st k�zvetlen�l a telep�t�s
  mapp�j�b�l.

o Ha a Firebird alkalmaz�sk�nt fut (szolg�ltat�s
  helyett), aj�nlatos k�zzel le�ll�tani a szervert
  miel�tt  futtatja az elt�vol�t� programot. Ez az�rt
  aj�nlatos,  mert az elt�vol�t� program nem k�pes
  le�ll�tani az alkalmaz�sk�nt fut� szervert. Ha a
  szerver fut elt�vol�t�s alatt, az elt�vol�t�s nem
  lesz t�k�letes. A visszamarad� r�szeket �nnek kell
  majd elt�vol�tania.

o Az elt�vol�t�si elj�r�s az al�bbi f�jlokat hagyja a
  a telep�t�si mapp�ban:

  - aliases.conf
  - firebird.conf
  - firebird.log
  - security2.fdb

  Ez az eredm�ny sz�nd�kos. Ezek a f�jlok a felhaszn�l�k
  �ltal m�dos�that�k �s sz�ks�g lehet r�juk a j�v�ben,
  a Firebird �jratelep�t�se sor�n. Amennyiben nincs
  sz�ks�ge ezekre a f�jlokra, �gy le is t�r�lheti
  �ket.


Megjegyz�sek
------------

  A Firebird ig�nyli a WinSock2 kiterjeszt�s megl�t�t.
  Minden Win32 platform elvileg tartalmazza ezt a
  kieg�sz�t�st a Windows 95-�t kiv�ve. A telep�t�
  ellen�rzi a WinSock2 kieg�sz�t�s megl�t�t, s ha nem
  tal�lja, �gy a telep�t�s meghi�sul. A k�vetkez�
  linken:

  http://support.microsoft.com/default.aspx?scid=kb;EN-US;q177719

  megtudhatja, hogyan juthat hozz� ehhez a
  kiterjeszt�shez.


Telep�t�s k�tegelt (batch) f�jlb�l
----------------------------------

A telep�t�program futtathat� k�tegelt (batch) f�jlb�l
is. Inform�ci�kat a k�vetkez� dokumentumb�l kaphat:

	installation_scripted.txt
