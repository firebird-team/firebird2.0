#!/bin/sh
#
# Repack upstream source converting from bz2 to gz and
# removing some debian-supplied libraries and generated
# files in the process
#
# To be called via debian/watch (uscan or uscan --force)
# or
#  sh debian/repack.sh --upstream-version VER FILE

set -e
set -u

usage() {
    cat <<EOF >& 2
Usage: $0 --upstream-version VER FILE

            or

       uscan [--force]
EOF
}

[ "${1:-}" = "--upstream-version" ] \
    && [ -n "${2:-}" ] \
    && [ -n "${3:-}" ] \
    && [ -z "${4:-}" ] \
    || usage

TMPDIR=`mktemp -d -p .`

trap "rm -rf $TMPDIR" INT QUIT 0

VER="$2"
DEB_VER="${VER}.ds2"
UP_VER="${VER}"
UPSTREAM_TAR="$3"
UPSTREAM_DIR=Firebird-${UP_VER}
ORIG="../firebird2.0_${DEB_VER}.orig.tar.gz"
ORIG_DIR="firebird2.0-${DEB_VER}.orig"

if [ -e "$ORIG" ]; then
    echo "$ORIG already exists. Aborting."
    exit 1
fi

echo -n "Expanding upstream source tree..."
tar xjf $UPSTREAM_TAR -C $TMPDIR
echo " done."

# clean sources, needlessly supplied by upstream.
# Debian has packages for them already,
# generated files and license-less copyrighted works
echo "Cleaning upstream sources from bundled libraries things..."
for d in extern/icu extern/editline extern/regex ;
do
    echo "  $d"
    rm -r $TMPDIR/$UPSTREAM_DIR/$d
done

echo "Removing generated files..."
for d in src/dsql/parse.cpp ;
do
    echo "  $d"
    rm -r $TMPDIR/$UPSTREAM_DIR/$d
done

echo "Remmoving copyrighted files without alicense..."
for d in src/install/arch-specific/solx86gcc/CS/postinstall.in \
    src/install/arch-specific/solx86gcc/CS/postremove.in \
    src/install/arch-specific/solx86gcc/CS/preinstall.in \
    extern/SfIO/include ;
do
    echo "  $d"
    rm -r $TMPDIR/$UPSTREAM_DIR/$d
done


mv $TMPDIR/$UPSTREAM_DIR $TMPDIR/$ORIG_DIR

echo -n Repackaging into ${ORIG} ...
tar c -C $TMPDIR $ORIG_DIR | gzip -n -9 > "$ORIG"
echo " done."
