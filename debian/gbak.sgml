<!doctype refentry PUBLIC "-//OASIS//DTD DocBook V4.1//EN" [

<!-- Process this file with docbook-to-man to generate an nroff manual
     page: `docbook-to-man manpage.sgml > manpage.1'.  You may view
     the manual page with: `docbook-to-man manpage.sgml | nroff -man |
     less'.  A typical entry in a Makefile or Makefile.am is:

manpage.1: manpage.sgml
	docbook-to-man $< > $@
  -->

<!-- This example was constructed by Colin Watson
     <email>cjwatson@debian.org</email>, based on a man page template
     provided by Tom Christiansen <email>tchrist@jhereg.perl.com</email>
     and a DocBook man page example by Craig Small
     <email>csmall@debian.org</email>.
  -->

  <!-- Fill in the various UPPER CASE things here. -->
  <!ENTITY manfirstname "<firstname>Damyan</firstname>">
  <!ENTITY mansurname   "<surname>Ivanov</surname>">
  <!-- Please adjust the date whenever revising the manpage. -->
  <!ENTITY mandate      "<date>June 21 2004</date>">
  <!-- SECTION should be 1-8, maybe with subsection. Other parameters are
       allowed: see man(7), man(1). -->
  <!ENTITY mansection   "<manvolnum>1</manvolnum>">
  <!ENTITY manemail     "<email>dam@modsoftsys.com</email>">
  <!ENTITY manusername  "Damyan Ivanov">
  <!ENTITY manucpackage "<refentrytitle>GBAK</refentrytitle>">
  <!ENTITY manpackage   "gbak">
]>

<refentry>
  <refentryinfo>
    <address>
      &manemail;
    </address>
    <author>
      &manfirstname;
      &mansurname;
    </author>
    <copyright>
      <year>2004</year>
      <holder>&manusername;</holder>
    </copyright>
    &mandate;
  </refentryinfo>
  <refmeta>
    &manucpackage;

    &mansection;
  </refmeta>
  <refnamediv>
    <refname>&manpackage;</refname>

    <refpurpose>Backup/restore Firebird database</refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <cmdsynopsis>
      <command>&manpackage;</command>
      <arg choice="req"> -b</arg>
      <arg rep="repeat" choice="opt">backup option</arg>
      <arg rep="repeat" choice="opt">common option</arg>
      <arg choice="req"><replaceable>source</replaceable></arg>
      <arg choice="req"><replaceable>target</replaceable></arg>
    </cmdsynopsis>
    <cmdsynopsis>
      <command>&manpackage;</command>
      <arg choice="req"> -c|-r</arg>
      <arg rep="repeat" choice="opt">restore option</arg>
      <arg rep="repeat" choice="opt">common option</arg>
      <arg choice="req"><replaceable>source</replaceable></arg>
      <arg choice="req"><replaceable>target</replaceable></arg>
    </cmdsynopsis>
  </refsynopsisdiv>
  <refsect1>
    <title>DESCRIPTION</title>

    <para><command>&manpackage;</command> is the tool for managing Firebird
    database backup and restore.</para>

    <para>In backup mode <replaceable>source</replaceable> is an existing
    database path (may include server name) and
    <replaceable>target</replaceable> is backup file name. It could be also a
    special device name like /dev/stdout (to redirect backup data to STDOUT) or
    /dev/st to put the backup on tape.</para>

    <para>In restore mode <replaceable>source</replaceable> is existing backup
    file (or special device like /dev/stdin for STDIN or /dev/st to get the
    backup from tape) and <replaceable>target</replaceable> is path to firebird
    database and may include server name.</para>

    <para>
    All options can be abbreviated. Below both full versions of options and the
    shortest possible versions are given.
    </para>

</refsect1>
<refsect1>
  <title>COMMON OPTIONS</title>
    <refsect2>
      <title>-user <replaceable>username</replaceable></title>
      <para>User name to use when connecting to database. Overrides
      environment variable. See
      <citerefentry><refentrytitle>ENVIRONMENT</refentrytitle></citerefentry>.</para>
    </refsect2>

    <refsect2>
      <title>-ro[le] <replaceable>role_name</replaceable></title>
	<para>SQL role to use when connecting to database. Overrides
	environment variable. See
	<citerefentry><refentrytitle>ENVIRONMENT</refentrytitle></citerefentry>.</para>
    </refsect2>

    <refsect2>
      <title>-pas[sword] <replaceable>password</replaceable></title>
	<para>Password to use when connecting to database. Overrides
	environment variable. See
	<citerefentry><refentrytitle>ENVIRONMENT</refentrytitle></citerefentry>.</para>
    </refsect2>

    <refsect2>
      <title>-se[rvice]</title>
	<para>Use services manager when talking to a database.</para>
    </refsect2>

    <refsect2>
      <title>-v[erify]</title>
	<para>Report each action.</para>
	<para>Displays verbose information about what metadata is being
	processed, what table is being backed up/restores and how many records
	are processed.</para>
    </refsect2>

    <refsect2>
      <title>-y <replaceable>path</replaceable></title>
	<para>Redirect status/error messages to the given path.</para>
    </refsect2>

    <refsect2>
      <title>-z</title>
	<para>Report full version number before doing anything else.</para>
    </refsect2>
</refsect1>

<refsect1>
  <title>BACKUP OPTIONS</title>

  <refsect2>
    <title>-b[ackup_database]</title>
	<para>Turn on backup mode.</para>
  </refsect2>

  <refsect2>
    <title>-co[nvert]</title>
    <para>Convert external tables into tables.</para>
    <para>
	Normally external tables are ignored during backup and restore. With
	<option>-convert</option> option external tables are included in
	the backup as any other regular table. When such a backup is restored,
	these previously external tables are created as regular tables.</para>
  </refsect2>

  <refsect2>
      <title>-fa[ctor] <replaceable>num</replaceable></title>
      <para>Blocking factor. Output is written in block of
          <replaceable>num</replaceable> bytes. Useful when backing up to a
          tape.</para>
  </refsect2>

  <refsect2>
    <title>-g[arbage_collect]</title>
    <para>Inhibit garbage collection.</para>
    <para>Normally Firebird runs a garbage collector when reading through
    the tables. Since the backup process reads all the tables, this forces
    garbage collection of the whole database. In case you make the backup
    just to restore it over the existing database this garbage collection is
    unnecessary. Another reason to use this options is if a garbage
    collection is engaged by other means - either by using gfix or the
    automatic sweep. Using this switch speeds up the backup process and also
    can help in some cases of backing up damaged databases.</para>
  </refsect2>

  <refsect2>
    <title>-ig[nore]</title>
    <para>Ignore records with bad checksums.</para>
    <para>This option should be used only when backing up damaged
    databases.</para>
  </refsect2>

  <refsect2>
    <title>-l[imbo]</title>
    <para>Ignore transactions in limbo.</para>
    <para>This switch causes the backup process to ignore transactions in
    limbo state.</para>
  </refsect2>

  <refsect2>
    <title>-m[etadata_only]</title>
    <para>Backup metadata only.</para>
    <para>With this option only the metadata is written to the backup. This
    includes all tables/views/triggers/indexes/stored
    procedures/domains/generators etc. No table data is included in backup.
    Such a backup is useful for creating an "empty" copy of a
    database.</para>
  </refsect2>

  <refsect2>
    <title>-nt</title>
    <para>Use non-transportable backup format.</para>
    <para>By default a "transportable" backup format is used. This means you
    can backup a database on little-endian machine and restore it on
    big-endian machine and vice versa. By using <option>-nt</option> the
    backup is only usefull on machines with the same "endianness".</para>
  </refsect2>

  <refsect2>
    <title>-ol[d_descriptions]</title>
    <para>save old-style metadata descriptions</para>
  </refsect2>
</refsect1>

<refsect1>
  <title>RESTORE OPTIONS</title>
  
  <refsect2>
    <title>-c[reate_database]</title>
    <para>Turn on restore mode.</para>
    <para><replaceable>target</replaceable> database will be created and
    must not already exist.</para>
  </refsect2>

  <refsect2>
    <title>-r[ecreate_database] [o[verwrite]]</title>
    <para>Like <option>-create_database</option>, but by adding
        <option>overwrite</option> one can force replacement of the target
        database.</para>
  </refsect2>

  <refsect2>
    <title>-rep[lace_database]</title>
    <para>Like <option>-create_database</option>, but the target is overwritten
        if it exists.</para>
  </refsect2>

  <refsect2>
    <title>-bu[ffers] <replaceable>num</replaceable></title>
    <para>When creating target database, ignore information about database
    buffers stored in the backup and use specified number of buffers.</para>
  </refsect2>

  <refsect2>
    <title>-e[xpand]</title>
    <para>Normally, Firebird uses RLE compression for strings when storing them
    on disk. This option turns this compression off.</para>
  </refsect2>

  <refsect2>
    <title>-i[nactive]</title>
    <para>Restore indexes in inactive state. This can be used to restore a
    backup, containing data inconsistent with foreign key/unique
    constraints.</para>
  </refsect2>

  <refsect2>
    <title>-k[ill]</title>
    <para>Do not create shadows when restoring.</para>
    <para>Normally and database shadows are created during restore. This option
    inhibits this.</para>
  </refsect2>

  <refsect2>
    <title>-mo[de] <replaceable>access</replaceable></title>
    <para>Restore database with given <replaceable>access</replaceable>.</para>
    <para>Valid values for <replaceable>access</replaceable> are
    <option>read_only</option> and <option>read_write</option>. Default is to
    restore the database with its original access mode.</para>
  </refsect2>

  <refsect2>
    <title>-n[o_validity]</title>
    <para>Restore database without its validity constraints. This includes
    foreign key, unique and check constraints. Usefull for restoring a backup,
    containing inconsistent data.</para>
  </refsect2>

  <refsect2>
    <title>-o[ne_at_a_time]</title>
    <para>Commit restoring transaction after restoring each table.</para>
  </refsect2>

  <refsect2>
    <title>-p[age_size] <replaceable>num</replaceable></title>
    <para>Override page size stored in the backup.  Valid values for
    <replaceable>num</replaceable> are 1024, 2048, 4096, 8192, 16384 and
    32768.</para>
  </refsect2>

  <refsect2>
    <title>-use_[all_space]</title>
    <para>Normally, Fireburd reserves some space on each data page for further
        use. This reserved space is used for newly inserted data and for
        keeping older versions of the data. Having space reserved for this
        purpose "near" to the data speeds up modifications. If the database
        will used mainly for read operations, specifying
        <option>-use_all_space</option> will save some space.</para>
    <para>This option is particularly useful when restoring a read-only
        database, since read-only databases do not need any additional space
        for back-versions.</para>
  </refsect2>
</refsect1>
<refsect1>
  <title>RESTORING TO MULTIPLE-FILE DATABASE</title>
  <para>Previously, Firebird was not able to work with 64-bit file pointers
  thus limiting databases to (about) 2GB of size per database.</para>
  <para>Since 2GB is not much of data since years, there is a mechanism for
  spreading the database into multiple files. This way you can have multiple
  2GB files, containing all your data.</para>
  <para>When restoring to multiple-file database the target argument is of
  form: <replaceable>file_1</replaceable> <replaceable>pages_1</replaceable>
  <replaceable>file_2</replaceable> <replaceable>pages_2</replaceable> ...
  <replaceable>file_N-1</replaceable> <replaceable>pages_N-1</replaceable>
  <replaceable>file_N</replaceable></para>
  <para>Each <replaceable>pages_n</replaceable> specifies at most how many
  pages to put in <replaceable>file_n</replaceable>.  Last in the list is a
  filename without page limit - it will contain all the pages not fit in
  <replaceable>file_N-1</replaceable>. Note that
  <replaceable>pages_n</replaceable> is measured in pages, not bytes so the
  maximum possible number depends on page size.</para>
  <para>Imagine you have a filesystem (or an ancient OS) which only supports
  32-bit file pointers. To play safe, you decide to split your database on 2GB
  files. If the page size for the database is 8192, then each file can have up
  to 2*1024*1024*1024/8192 = 262144 pages.</para>

  <refsect2>
    <title>NOTE</title>
    <para>Note that on a recent OS and filesystem Firebird can use 64-bit file
    pointers rendering multi-file databases unnecessary.</para>
  </refsect2>
</refsect1>
    
<refsect1>
  <title>ENVIRONMENT</title>
  <para>As all other Firebird utilities, gbak accepts following environment
  variables:</para>
  <variablelist>
    <varlistentry>
      <term>ISC_USER</term>
      <listitem><para>default value for <option>-user</option> option</para></listitem>
    </varlistentry>
    <varlistentry>
      <term>ISC_ROLE</term>
      <listitem><para>default value for <option>-role</option> option</para></listitem>
    </varlistentry>
    <varlistentry>
      <term>ISC_PASSWORD</term>
      <listitem><para>default value for <option>-password</option> option</para></listitem>
    </varlistentry>
  </variablelist>
</refsect1>

<refsect1>
  <title>EXAMPLES</title>
  
  <para>In all examples <option>-user</option>, <option>-role</option> and
  <option>-password</option> options are omitted for clarity.  In a real world
  situation they (or their corresponding enviromnemt variables) should be
  used.</para>
    
  <para>Note that filename extensions used here are just recommended. Using
  unified extensions scheme helps guess file type just by looking at its
  extension.</para>

  <para>Here are some commonly used extensions:</para>
  <variablelist>
    <varlistentry>
      <term>.fdb</term>
      <listitem><para>Firebird database</para></listitem>
    </varlistentry>
    <varlistentry>
      <term>.gdb</term>
      <listitem><para>Firebird database, legacy extension from the days when
      Firebird was Interbase. gdb actually comes from Grotton database, named
      after the company that created the software back in
      1984.</para></listitem>
    </varlistentry>
    <varlistentry>
      <term>.fdb.2</term>
      <listitem><para>Second file of multi-file database</para></listitem>
    </varlistentry>
    <varlistentry>
      <term>.fdb.3</term>
      <listitem><para>Third file of multi-file database</para></listitem>
    </varlistentry>
    <varlistentry>
      <term>.fdb.<replaceable>N</replaceable></term>
      <listitem><para><replaceable>N</replaceable>-th file of multi-file database</para></listitem>
    </varlistentry>
    <varlistentry>
      <term>.fbk</term>
      <listitem><para>Firebird backup file</para></listitem>
    </varlistentry>
    <varlistentry>
      <term>.gbk</term>
      <listitem><para>Legacy extension for backup file</para></listitem>
    </varlistentry>
    <varlistentry>
      <term>.fbk.gz</term>
      <listitem><para>Gzipped backup file</para></listitem>
    </varlistentry>
  </variablelist>
  <refsect2>
    <title>Backup a database into a compressed format:</title>
    <para>gbak -b db-srv:/database.fdb /dev/stdout | gzip > /file.fbk.gz</para>
  </refsect2>
  <refsect2>
    <title>Restore a database into new filename:</title>
    <para>zcat /file.fbk.gz | gbak -c /dev/stdin db-srv:/new-database.fdb</para>
  </refsect2>
</refsect1>


<refsect1>
  <title>AUTHOR</title>

  <para>This manpage was written by &manusername; &lt;&manemail;&gt; for Debian
      GNU/Linux but may be used by others. Permission is granted to use this
      document, with or without modifications, provided that this notice is
      retained. If we meet some day, and you think this stuff is worth it, you
      can buy me a beer in return.</para>
</refsect1>
<refsect1>
  <title>COPYRIGHT</title>
  <para>&copy; 2004,2007 Damyan Ivanov</para>
</refsect1>

</refentry>

